#!/bin/bash
# Examples: ./topics.sh --create --topic mytopic
#           ./topics.sh --describe --topic mytopic
#
IP="$(docker-machine ip dev)"
ZKC="$IP:2181,$IP:2182,$IP:2183"

CMD="\$KAFKA_HOME/bin/kafka-topics.sh --zookeeper \$ZK $@"
echo "***** Command *****"
echo $CMD
echo ""
docker run --rm -ti -e ZK="$ZKC" kafka bash -c "$CMD"
