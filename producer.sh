#!/bin/bash
# Example: producer.sh --topic mytopic
#
IP="$(docker-machine ip dev)"
ZKC="$IP:2181,$IP:2182,$IP:2183"

CMD="\$KAFKA_HOME/bin/kafka-console-producer.sh --broker-list=$IP:9092 $@"
echo "***** Command *****"
echo $CMD
echo ""
docker run --rm -ti -e ZK="$ZKC" kafka bash -c "$CMD"
