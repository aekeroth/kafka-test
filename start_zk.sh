#!/bin/bash
IP="$(docker-machine ip dev)"
ZK_SERVERS="server.1=$IP:2888:3888 server.2=$IP:2889:3889 server.3=$IP:2890:3890"
echo $ZK_SERVERS
docker run -d -e ZK_SERVERS="$ZK_SERVERS" -e ZK_ID=1 -p 2181:2181 -p 2888:2888 -p 3888:3888 --name ZK1 zookeeper
docker run -d -e ZK_SERVERS="$ZK_SERVERS" -e ZK_ID=2 -p 2182:2181 -p 2889:2889 -p 3889:3889 --name ZK2 zookeeper
docker run -d -e ZK_SERVERS="$ZK_SERVERS" -e ZK_ID=3 -p 2183:2181 -p 2890:2890 -p 3890:3890 --name ZK3 zookeeper
