#!/bin/bash
IP="$(docker-machine ip dev)"
for i in {1..3};
    do
    echo "***** ZK $i *****";
    port=$((2180 + $i))
    echo mntr | nc $IP $port;
    echo ""
    done
